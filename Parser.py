import json
from datetime import datetime
import requests
import sys
import os
from dotenv import load_dotenv
import sqlite3
load_dotenv()
TOKEN = os.getenv('TOKEN')
currentdir = os.path.dirname(os.path.realpath(__file__))
db_path = os.path.join(currentdir, "tentorg.db")
sys.path.append(db_path)
sys.path.append("/home/manage_report")

currentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB


class Parser:
    def __init__(self, parser_name: str):
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_content()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def add_order_to_db(self, number_order):
        try:
            with sqlite3.connect(db_path) as db:
                sql = db.cursor()
                sql.execute(f"""INSERT INTO orders(id) VALUES ({number_order})""")
                db.commit()
        except Exception as e:
            print(f'{e} - err')

    def get_last_order(self):
        with sqlite3.connect(db_path) as db:
            try:
                sql = db.cursor()
                sql.execute(f"SELECT max(id) FROM orders")
                tuple_order: tuple = sql.fetchone()
                number_order = tuple_order[0]
                return number_order
            except Exception as e:
                print(f'{e} - read')
                sql.execute("""CREATE TABLE orders (id integer unique);""")
                return None

    def get_json(self):
        ads = []
        page = 1
        url = f"https://api.tentorg.ru/openapi/order?page={page}"
        headers = {
            'Authorization': f'Bearer {TOKEN}'
        }
        last_order = self.get_last_order()
        select_id = 0
        while select_id != last_order:
            response = requests.get(url=url, headers=headers)
            data = response.json()
            for item in data:
                prop = json.loads(item.get('json_property'))
                prop_id = prop.get("id")
                select_id = prop_id

                if last_order is None:
                    self.add_order_to_db(prop_id)
                    ads.append(prop)
                    return ads

                if prop_id > last_order:
                    ads.append(prop)
                else:
                    return ads
            page += 1
        return ads

    def get_content(self):
        content = []
        ads = self.get_json()
        for item in ads:
            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'lots': [],
                'attachments': [],
                'procedureInfo': {
                    'endDate': ''
                },
                'customer': {
                    'fullName': '',
                    'factAddress': '',
                    'inn': 0,
                    'kpp': 0,
                },
                'contactPerson': {
                    'lastName': '',
                    'firstName': '',
                    'middleName': '',
                    'contactPhone': '',
                    'contactEMail': '',
                }}
            item_data['purchaseNumber'] = str(self.get_purchase_number(item))
            item_data['title'] = str(self.get_title(item))
            item_data['purchaseType'] = str(self.get_purchase_type(item))
            item_data['customer']['fullName'] = str(self.get_full_name(item))
            item_data['customer']['inn'] = int(self.get_inn(item))
            item_data['procedureInfo']['endDate'] = self.get_end_date(item)
            last_name, first_name, middle_name = self.get_name(item)
            item_data['contactPerson']['lastName'] = str(last_name)
            item_data['contactPerson']['firstName'] = str(first_name)
            item_data['contactPerson']['middleName'] = str(middle_name)

            item_data['contactPerson']['contactEMail'] = str(self.get_contacts(item))

            item_data['url'] = 'https://trade.tentorg.ru/sale/view-order?id=' + str(self.get_purchase_number(item))
            item_data['lots'] = self.get_lots(item)
            content.append(item_data)
        return content

    def get_title(self, data):
        try:
            name = data.get("items")
            if name is None:
                title = ''
            else:
                title = ''.join(''.join(str(name).split("'name': '")[1]).split("',")[0])
        except:
            title = ''
        return title

    def get_purchase_type(self, data):
        try:
            purchase_type = data.get("type")
        except:
            purchase_type = ''
        return purchase_type

    def get_purchase_number(self, data):
        try:
            number = data.get("id")
        except:
            number = ''
        return number

    def get_end_date(self, data):
        try:
            date = datetime.fromtimestamp(data.get("tender_end")).strftime('%H.%M.%S %d.%m.%Y')
        except:
            date = None
        return date

    def get_full_name(self, data):
        try:
            organizer_data = data.get("company")
            name = organizer_data.get("full_name")
        except:
            name = ''
        return name

    def get_inn(self, data):
        try:
            organizer_data = data.get("company")
            inn = organizer_data.get("inn")
            if not inn or inn is None:
                inn = 0
        except:
            inn = 0
        return inn

    def get_name(self, data):
        name_data: dict | None = data.get("executor", None)
        try:
            last_name = name_data.get('last_name')
        except:
            last_name = ''
        try:
            first_name = name_data.get('first_name')
        except:
            first_name = ''
        try:
            middle_name = name_data.get('middle_name')
        except:
            middle_name = ''

        return last_name, first_name, middle_name

    def get_contacts(self, data):
        contact_data: dict | None = data.get("executor", None)
        if isinstance(contact_data, dict):
            try:
                email = contact_data.get('email')
                if email is None:
                    email = ''
            except:
                email = ''
        else:
            email = ''
        return email

    def get_lots(self, data):
        try:
            lots_data = []
            lots = {
                'address': '',
                'price': '',
                'lotItems': []
            }
            adress_data: dict | None = data.get("orderObjects")
            adress = adress_data.get("address")
            if adress is None:
                adress = ''
            else:
                adress = adress

            lots['address'] = str(adress)

            name = data.get("items")
            if name is None:
                result_name = ''
            else:
                result_name = ''.join(''.join(str(name).split("'full_name': '")[1]).split("',")[0])

            names = {
                'code': '1',
                'name': result_name}

            lots['lotItems'].append(names)
            lots_data.append(lots)
        except:
            return []
        return lots_data
