import logging
import os
import sys
from pathlib import Path
from dotenv import load_dotenv

PRODUCTION = True
if PRODUCTION:
    current_dir = os.path.dirname(os.path.realpath(__file__))
    base_path = os.path.dirname(current_dir)
    sys.path.append(base_path)
    sys.path.append("/home/manage_report")
    dotenv_path = Path("/home/service/.env")

else:
    dotenv_path = Path('.env')

load_dotenv(dotenv_path=dotenv_path)
CHROMEDRIVER_PATH = os.getenv("CHROMEDRIVER_PATH")

# прокси
PROXIES = {
    "http": os.getenv("PROXIES_HTTP"),
    "https": os.getenv("PROXIES_HTTPS"),
}


logging.basicConfig(
    level=logging.INFO,
    filename="parser.log",
    format="%(asctime)s - [%(levelname)s]: %(message)s",
)
